# Developer Notes

## Maven
* Setting finalName tag on build section of the pom sets the final name of the jar. 😁

## JPA / Hibernate
* When setting cascade configurations on Entities with relationships, **MERGE** is the safest option.
  * Changes to the records would only cascade when the persistence context would have related changes on the same session.
  * **Create UserLogin** -> **Create UserRole** -> **Add Role to Login** -> **Save Login** -> **Save Role** -> **Create Profile** -> **Set Profile to Login** -> **Save Profile** -> **Save Login**
