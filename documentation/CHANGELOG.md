# CHANGELOG:

This file describes the features implemented for the application in chronological order - The first on the top being the first entry of changes.

It also contains the list of features that are not yet implemented, but are considered essential features, and  are planned to be included in the final product.

Each entry follows this specific pattern:

[date_stamp] [feature_name] [details]

Please check out the lists below:

## DONE

[date_stamp] [feature_name] [details]


## TODOs

[feature_name] - [details] - [date_stamp (planned)] - [module]

* **Data Models** - Classes and Objects used to represent the data that would be manipulated by the application. It includes data to be persisted in database tables, default values for use in data entry (and validation), or configurable values generated from business logic.
  * **Database Entities** - Objects used to represent tables and fields for persistence 
  * **DTOs** - Data objects to be used in request / response data models
  * **Enumerations** - Dropdown selection values, radio box / check box options, default expected field values (for validation), etc.
* **Controllers** - Used for configuring REST, as well as regular Web endpoint URLs, their expected media types for requests and responses, the HTTP methods for the requests, URL request mappings for each, routing and forwarding configuration, and response data generation and rendering.
* **Static (and Dynamic) Web Pages and Resources** - Web pages that contain static content, dynamic web page generation logic, their referenced media (images, videos, etc.) 
* **Repositories** - Used for configuring queries to database tables (configured from the DB Entities). Contains default (CRUD) and custom queries for adding, viewing, updating, and deleting rows from tables.
* **Services** - Used for configuring logic for presenting fetched / manipulated data (from Repositories), parsing and mapping input (from Controllers), as well as other additional business logic that needs to be performed (validation, integration calls to other systems, authentication, etc.)
* **Tests** - Pieces of code that contain logic to test each component's functionality as a single unit, when integrated with other components, as well as testing the overall performance of the application. Each test case would confirm whether the components are working as intended, and how the components would behave when put in various scenarios.
* **Configuration** - Used for configuring components to be utilized by the application, such as the following:
  * Helper / Utility Classes
    * Serializers / Deserializers
  * Data Mappers
    * POJO to JSON mappers
    * POJO to DB Entity mappers
    * POJO to XML mappers (if needed)
  * Data Validation - Used for validating the input values for fields. Can be set immediately from the Controller / Presentation layer, or as deep as the Repository / Persistence layer (depending on the requirements). Basic validation includes the following:
    * Null / empty / blank value checking (for required fields)
    * Date / Time validation (for date / time ranges, creation timestamps, and others)
    * Email validation (pattern formatting)
    * Min / Max value validation (applicable for counting date / time ranges, duration values, currency, and other quantifiable data)
  * Spring Security (authentication, authorization, session management)
  * JWT token generation
    * Token creation
    * Token refresh
    * Token invalidation
  * Web and Application Context Configuration
    * Web Root Context Path
    * Static Resource Path Configuration
    * Exception / Error Handling and Error Response Generation
  * Logging
  * Build and Dependency Management - Configuring the libraries and APIs referenced by the application, and the location/s on which to fetch these dependencies. Also includes the configuration of processes to perform for each build 'step'.
    * Java
      * Maven
      * Gradle
    * Javascript and related frameworks
      * NPM / Yarn
    * Python
      * Pip / Virtualenv
  * Containerization - This includes configuration for creating Virtual Machine 'Containers' on which to deploy
  * Others 
