# FEATURES

- Data Model
- Repository / Table Design
- Service Classes
- Controller / Endpoint Logic
- Exception Handling
- Utility Classes
- Test Cases
- Resource Configuration