# Attendance Management Demo Application
### v. 0.0.1-SNAPSHOT

## Getting Started

This application comprises of several modules:

**A. Attendance Management Demo APIs** - the APIs that are responsible for the business logic of the project
  1. As per spec found at [(Employee Time Tracker Word Document)](documentation/Employee Time Tracker.docx), the APIs would enable the user to capture and track their daily tasks and working hours.
  2. Users can add, modify, and delete task detail entries for each day. Sample task detail provided below:

     - *John Doe, 25/04/2022 10-11AM, Meeting, Standup meeting with the team*
     - *John Doe, 25/04/2022 11-01PM, Training, Hands-on training on Python*
  3. The application should have the capacity to display the total duration (in hours) for any period specified: ***Daily, Weekly, and Monthly.***
  4. The application should also have **Role-Based Access Control (RBAC)**, enabling the validation of regular users ***(Associate)***, as well as moderators/administrators ***(Administrator)*** access to the APIs exposed by the application.
       * Each user should only be allowed to perform actions based on their role. 
    
         ***EXAMPLE:***  
         - ***Associates** would be able to create, update, view, and delete their ***own task and hour entries ONLY****

         - ***Admins** would be able to view charts and graphs for every Associate, perform adjustments on them ***(update, delete, replace)***, but ***would NOT be able to enter task/hour entries on behalf of the Associate*** for the current day.*
       * **Duplicate entries with the same date and time duration** are **NOT** allowed (for tasks that are being done in parallel).
       * **Total hours duration per day should not exceed 8 hours for any task.**
  5. The application would also have the capacity to transform entry data for rendering presentations or reports, such as pie charts, and bar graphs.
     * For regular users, Daily tasks/hours data should be available to be rendered in a pie chart.
     * For regular users, Weekly and Monthly tasks/hours data should be available to be rendered on bar graphs.
     * Administrators should have access to these charts and graphs of user tasks/hours (Daily, Weekly, Monthly), and would be able to make adjustments, if necessary.

**B. OAuth2 Test Authorization Server Application** - an instance of an OAuth2 Authorization Server running on Spring Boot. Provides the functionality to authorize authentication requests using JWT tokens.

**C. OAuth2 Test Resource Server Application** - an instance of an OAuth2 Resource Server, which provides the functionality to generate Bearer Tokens for use by Attendance Management Demo API users.

## How to Start the Services

The Attendance Management APIs and test classes are located on the root source directories for the project. The Authorization Server and Resource Server applications are located on separate sub-directories within the project.

To run the applications locally, please do the following preparations:
* Prepare and configure pre-requisite software. Latest versions are preferred:
  * Git
  * Maven
  * Any Rest API client **(SoapUI, Postman, etc.)**
  * Any Java JDK **(8 and above)** - **NOTE:** All code from all modules are compatible with, and are configured to be compiled and run using **Java 11** specs. 
  * Your preferred database **(MySQL, Oracle, PostgreSQL, MSSQL)**  -  **NOTE:** Attendance Management Demo has been tested with **H2 embedded** for local/test environment configuration, and **PostgreSQL standalone** for higher environment configurations. 
  * And their corresponding database clients (or you can use **DBeaver** as universal db client)
  * IDE of choice (**Eclipse, IntelliJ Ultimate/Community, VSCode,** etc.)
* Clone the repository contents to your local storage. And open the cloned copy on your IDE.
* To start the Attendance Management APIs, run the [AttendanceManagementDemoApplication.java](src/main/java/net/garapata/api/attendance/AttendanceManagementDemoApplication.java) class.
* To start the Authorization Server, run the [test-authorization-server TestOAuth2AuthorizationServerApplication.java](test-authorization-server/src/main/java/net/garapata/TestOAuth2AuthorizationServerApplication.java) class.
* To start the Resource Server, run the [test-resource-server TestResourceServerApplication.java](test-resource-server/src/main/java/net/garapata/TestResourceServerApplication.java) class.
* The Authorization and Resource Servers are pre-configured to run with minimal re-configuration needed. Just download their Maven dependencies, and they are set to run. 
* Endpoint URLs and other relevant information for both modules are available from their respective README Markdown documents [here](test-authorization-server/README.md), and [here](test-resource-server/README.md), respectively.
* Design documents, diagrams, and other documentation for the Attendance Management Demo APIs available [here](documentation).

<img src="./documentation/misc/garatech-logo-temp.png" style="width: 50px; height: 50px;">