package net.garapata.api.attendance.utils.common;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.extern.slf4j.Slf4j;
import net.garapata.api.attendance.model.TaskDetail;
import net.garapata.api.attendance.model.dto.CustomUserDetails;
import net.garapata.api.attendance.util.datetime.DateTimeUtils;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.json.AutoConfigureJson;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.time.DateTimeException;
import java.time.DayOfWeek;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Arrays;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.*;

@Slf4j
@ExtendWith(SpringExtension.class)
@ActiveProfiles(value = "test")
@AutoConfigureJson
public class CommonValuesTest {
    @Autowired
    ObjectMapper objectMapper;

    private static final Logger logger = LoggerFactory.getLogger(CommonValuesTest.class);

    @DisplayName("Given week test data builder method and weekday date range, when method is called, a list of TaskDetail objects would be created for the date ranges")
    @Test
    void testWeekBuilder(){
        // Given
        int year = 2023, month = 6, week_start = 5, week_end = 9;
        // When
        List<TaskDetail> firstWeekJune2023 = CommonValues.weekTestTaskDataBuilder(year, month, week_start, week_end);
        firstWeekJune2023.stream().forEach(System.out::println);
        // Then
        assertThat(firstWeekJune2023.contains(LocalDateTime.of(2023, 6, 7, 0, 0)));
    }

    @DisplayName("Given the a Monday date for a week, when calling method to get the last weekday for that week, the date returned is a Friday.")
    @Test
    void testGetWeekdayDateRanges(){
        // Given
        LocalDateTime mondayWeek = LocalDateTime.of(2024, 3, 4, 0, 0, 0);
        // When
        LocalDateTime fridayWeek = DateTimeUtils.getFridayForTheWeek(mondayWeek);
        System.out.println(DateTimeUtils.getWeekOfMonthDescription(mondayWeek));
        System.out.println(DateTimeUtils.getLongDescription(mondayWeek));
        System.out.println(DateTimeUtils.getLongDescription(fridayWeek));
        // Then
        assertTrue(fridayWeek.getDayOfWeek().equals(DayOfWeek.FRIDAY));
    }

    @DisplayName("Given month and year as input, when generating weekday date ranges for the month, list of dates representing Mondays for each week of the month would be returned")
    @Test
    void testMonthDateRangeGenerator() throws Exception {
        // Given = November (11), 2023. November 2023 has 5 weeks;
        List<LocalDate> testWeekRange=CommonValues.generateDateRange(2023, 11); //
        // When
        testWeekRange.stream().sorted().forEach((weekDate)->{
            LocalDate currDT = LocalDate.of(weekDate.getYear(), weekDate.getMonth(), weekDate.getDayOfMonth());
            System.out.println(DateTimeUtils.getWeekOfMonthDescription(currDT));
            System.out.println(DateTimeUtils.getLongDescription(currDT));
        });
        // Then
        assertEquals(testWeekRange.size(), 5);
    }

    @DisplayName("Given DaysOfWeek enum class, when iterating through the enumeration, displays day of week and ordinal value in the enumeration")
    @Test
    void testGetDaysOfWeekEnumValues() {
        // Given
        DayOfWeek[] weekVals = DayOfWeek.values();
        // When
        Arrays.stream(weekVals).forEach((day)-> System.out.println(day.ordinal() + " = " + day));
        // Then
        assertEquals(DayOfWeek.values().length, 7);
    }

    @DisplayName("Given date of March 4, 2024, when getting the long description string, date string contains \'Monday\'")
    @Test
    void testLongDescriptionFormatter(){
        // Given
        LocalDateTime testDate = LocalDateTime.of(2024, 3, 4, 11,30);
        // When
        System.out.println("Today's date is: " + DateTimeUtils.getLongDescription(testDate));
        // Then
        assertTrue(DateTimeUtils.getLongDescription(testDate).contains("Monday"));
    }


    @DisplayName("Given invalid date 2023 February 29, when set to a LocalDateTime, an LocalDateTimeException exception is thrown")
    @Test
    void testIfValidLastDay(){
        // Given
        // When
        Exception exception = assertThrows(DateTimeException.class, () -> LocalDate.of(2023, 2, 29));
        // Then
        assertTrue(exception.getMessage().contains("Invalid date"));
    }

    @DisplayName("Given UserDetails object extracted from a UserLogin credentials, when rendered to JSON, displays the authorities for the User")
    @Test
    void testCustomUserDetails() throws JsonProcessingException {
        // Given
        CustomUserDetails userDetails = CommonValues.CUSTOM_USER_DETAILS;
        // When
        System.out.println("UserDetails: " + objectMapper.writerWithDefaultPrettyPrinter().writeValueAsString(userDetails));
        // Then
        assertTrue(!userDetails.getAuthorities().isEmpty());
    }
}
