package net.garapata.api.attendance.model;

import net.garapata.api.attendance.model.enums.AccountStatus;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.util.Arrays;

public class AccountStatusTests {

    @DisplayName("Given integer index value, find AccountStatus from array")
    @Test
    void testAccountStatusValues(){
        // Given: Setup object or precondition
        Integer acctStatus = 1;
        AccountStatus status = null;

        // When: Action or behavior that we are going to test
        status = Arrays.stream(AccountStatus.values())
                .filter((value)-> value.ordinal() == acctStatus)
                .findFirst().get();


        // Then: Verify the output or expected result
        System.out.println(status.name());
        Assertions.assertNotNull(status);
    }
}
