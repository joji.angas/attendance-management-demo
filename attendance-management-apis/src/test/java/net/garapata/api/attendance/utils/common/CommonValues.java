package net.garapata.api.attendance.utils.common;

import net.garapata.api.attendance.model.TaskDetail;
import net.garapata.api.attendance.model.UserLogin;
import net.garapata.api.attendance.model.UserProfile;
import net.garapata.api.attendance.model.UserRole;
import net.garapata.api.attendance.model.dto.CustomUserDetails;
import net.garapata.api.attendance.model.enums.AccountStatus;
import net.garapata.api.attendance.model.enums.RoleType;
import net.garapata.api.attendance.util.datetime.DateTimeUtils;
import org.springframework.security.core.authority.SimpleGrantedAuthority;

import java.time.DayOfWeek;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.Month;
import java.util.*;
import java.util.stream.Collectors;


public class CommonValues {

    public static final UserLogin USER_LOGIN_RECORD = new UserLogin("jangas", "m4m3m1m0", "jangas@garapata.net");

    public static final UserProfile USER_PROFILE_RECORD = new UserProfile("Joji", "Angas", "Engineering", "Engineer", "1234567890", null);

    public static final Set<UserRole> ROLES = Arrays.asList(
            new UserRole(RoleType.ASSOCIATE.ordinal(), AccountStatus.ACTIVE.ordinal(), null),
            new UserRole(RoleType.ADMIN.ordinal(), AccountStatus.ACTIVE.ordinal(), null)
    ).stream().distinct().collect(Collectors.toSet());

    public static final CustomUserDetails CUSTOM_USER_DETAILS = CustomUserDetails.builder()
            .username(USER_LOGIN_RECORD.getUsername())
            .password(USER_LOGIN_RECORD.getPassword())
//            .isEnabled((USER_LOGIN_RECORD.getStatus()==AccountStatus.ACTIVE.ordinal()))
            .authorities(ROLES.stream()
                    .map(role -> new SimpleGrantedAuthority(RoleType.withRoleIndex(role.getRole_type()).getDescription()))
                    .collect(Collectors.toList())
            ).build();

    public static final List<UserProfile> test_accounts = Arrays.asList(
            USER_PROFILE_RECORD,
            UserProfile.builder()
                    .firstName("John Henry")
                    .lastName("Angas")
                    .position("Associate Developer")
                    .department("IT Services")
                    .phone("1234566666")
                    .build(),
            UserProfile.builder()
                    .firstName("Goku")
                    .lastName("Son")
                    .position("Godlike Hero")
                    .department("Hero")
                    .phone("9999999999")
                    .build()
    );

    public static final List<TaskDetail> tasks_for_today = Arrays.asList(
            TaskDetail.builder()
                    .employee_name("Joji Angas")
                    .project("Academy Training")
                    .role("Associate")
                    .task_category("Training")
                    .task_description("First session")
                    .time_start(LocalDateTime.of(2023, 02, 01, 9, 00))
                    .time_end(LocalDateTime.of(2023, 02, 01, 11, 00))
                    .build(),
            TaskDetail.builder()
                    .employee_name("Joji Angas")
                    .project("Academy Training")
                    .role("Associate")
                    .task_category("Break")
                    .task_description("Lunch")
                    .time_start(LocalDateTime.of(2023, 02, 01, 11, 00))
                    .time_end(LocalDateTime.of(2023, 02, 01, 12, 00))
                    .build(),
            TaskDetail.builder()
                    .employee_name("Joji Angas")
                    .project("Academy Training")
                    .role("Associate")
                    .task_category("Training")
                    .task_description("First session")
                    .time_start(LocalDateTime.of(2023, 02, 01, 12, 00))
                    .time_end(LocalDateTime.of(2023, 02, 01, 14, 00))
                    .build(),
            TaskDetail.builder()
                    .employee_name("Joji Angas")
                    .project("Academy Training")
                    .role("Associate")
                    .task_category("Self-Study/Practice")
                    .task_description("Coding Practice - Multithreading")
                    .time_start(LocalDateTime.of(2023, 02, 01, 14, 00))
                    .time_end(LocalDateTime.of(2023, 02, 01, 18, 00))
                    .build(),
            TaskDetail.builder()
                    .employee_name("Joji Angas")
                    .project("Academy Training")
                    .role("Associate")
                    .task_category("Miscellaneous")
                    .task_description("Test Excess Entry")
                    .time_start(LocalDateTime.of(2023, 02, 01, 18, 00))
                    .time_end(LocalDateTime.of(2023, 02, 01, 19, 00))
                    .build()
    );

    public static final List<TaskDetail> tasks_for_the_week = Arrays.asList(
            TaskDetail.builder()
                    .employee_name("Joji Angas")
                    .project("Academy Training")
                    .role("Associate")
                    .task_category("Training")
                    .task_description("First session")
                    .time_start(LocalDateTime.of(2023, 02, 02, 9, 00))
                    .time_end(LocalDateTime.of(2023, 02, 02, 11, 00))
                    .build(),
            TaskDetail.builder()
                    .employee_name("Joji Angas")
                    .project("Academy Training")
                    .role("Associate")
                    .task_category("Break")
                    .task_description("Lunch")
                    .time_start(LocalDateTime.of(2023, 02, 02, 11, 00))
                    .time_end(LocalDateTime.of(2023, 02, 02, 12, 00))
                    .build(),
            TaskDetail.builder()
                    .employee_name("Joji Angas")
                    .project("Academy Training")
                    .role("Associate")
                    .task_category("Training")
                    .task_description("First session")
                    .time_start(LocalDateTime.of(2023, 02, 02, 12, 00))
                    .time_end(LocalDateTime.of(2023, 02, 02, 14, 00))
                    .build(),
            TaskDetail.builder()
                    .employee_name("Joji Angas")
                    .project("Academy Training")
                    .role("Associate")
                    .task_category("Self-Study/Practice")
                    .task_description("Coding Practice - Multithreading")
                    .time_start(LocalDateTime.of(2023, 02, 02, 14, 00))
                    .time_end(LocalDateTime.of(2023, 02, 02, 18, 00))
                    .build(),
            TaskDetail.builder()
                    .employee_name("Joji Angas")
                    .project("Academy Training")
                    .role("Associate")
                    .task_category("Training")
                    .task_description("First session")
                    .time_start(LocalDateTime.of(2023, 02, 03, 9, 00))
                    .time_end(LocalDateTime.of(2023, 02, 03, 11, 00))
                    .build(),
            TaskDetail.builder()
                    .employee_name("Joji Angas")
                    .project("Academy Training")
                    .role("Associate")
                    .task_category("Break")
                    .task_description("Lunch")
                    .time_start(LocalDateTime.of(2023, 02, 03, 11, 00))
                    .time_end(LocalDateTime.of(2023, 02, 03, 12, 00))
                    .build(),
            TaskDetail.builder()
                    .employee_name("Joji Angas")
                    .project("Academy Training")
                    .role("Associate")
                    .task_category("Training")
                    .task_description("First session")
                    .time_start(LocalDateTime.of(2023, 02, 03, 12, 00))
                    .time_end(LocalDateTime.of(2023, 02, 03, 14, 00))
                    .build(),
            TaskDetail.builder()
                    .employee_name("Joji Angas")
                    .project("Academy Training")
                    .role("Associate")
                    .task_category("Self-Study/Practice")
                    .task_description("Coding Practice - Multithreading")
                    .time_start(LocalDateTime.of(2023, 02, 03, 14, 00))
                    .time_end(LocalDateTime.of(2023, 02, 03, 18, 00))
                    .build(),
            TaskDetail.builder()
                    .employee_name("Joji Angas")
                    .project("Academy Training")
                    .role("Associate")
                    .task_category("Training")
                    .task_description("First session")
                    .time_start(LocalDateTime.of(2023, 02, 04, 9, 00))
                    .time_end(LocalDateTime.of(2023, 02, 04, 11, 00))
                    .build(),
            TaskDetail.builder()
                    .employee_name("Joji Angas")
                    .project("Academy Training")
                    .role("Associate")
                    .task_category("Break")
                    .task_description("Lunch")
                    .time_start(LocalDateTime.of(2023, 02, 04, 11, 00))
                    .time_end(LocalDateTime.of(2023, 02, 04, 12, 00))
                    .build(),
            TaskDetail.builder()
                    .employee_name("Joji Angas")
                    .project("Academy Training")
                    .role("Associate")
                    .task_category("Training")
                    .task_description("First session")
                    .time_start(LocalDateTime.of(2023, 02, 04, 12, 00))
                    .time_end(LocalDateTime.of(2023, 02, 04, 14, 00))
                    .build(),
            TaskDetail.builder()
                    .employee_name("Joji Angas")
                    .project("Academy Training")
                    .role("Associate")
                    .task_category("Self-Study/Practice")
                    .task_description("Coding Practice - Multithreading")
                    .time_start(LocalDateTime.of(2023, 02, 04, 14, 00))
                    .time_end(LocalDateTime.of(2023, 02, 04, 18, 00))
                    .build()
    );

    public static List<LocalDate> generateDateRange(int year, int month) throws Exception {
        List<LocalDate> dateRange = new ArrayList<LocalDate>();
        int dateValue=getLastDayOfMonth(year, month);
        LocalDate firstday = LocalDate.of(year, month, 1);
        LocalDate currDate=LocalDate.of(year, month, dateValue);
        while(dateValue > 1){
            if(currDate.getDayOfWeek().equals(DayOfWeek.MONDAY)){
                dateRange.add(currDate);
            }
            dateValue--;
            currDate=LocalDate.of(year,month, dateValue);
        }
        if(!firstday.getDayOfWeek().equals(DayOfWeek.MONDAY)){
            dateRange.add(LocalDate.of(year, month, 1));
        }
        return dateRange;
    }

    public static LocalDateTime getLastWeekdayForTheWeek(LocalDateTime weekMonday){
        return DateTimeUtils.getFridayForTheWeek(weekMonday);
    }

    public static List<TaskDetail> weekTestTaskDataBuilder(int year, int month, int weekday_start, int weekday_end) {
        List<TaskDetail> weeklyTasks = new ArrayList<TaskDetail>();
        for (int x = weekday_start; x <= weekday_end; x++) {
            int curDay = x;
            tasks_for_today.stream().forEach((task) -> {
                LocalDateTime time_start = LocalDateTime.of(year, month, curDay, task.getTime_start().getHour(), task.getTime_start().getMinute());
                LocalDateTime time_end = LocalDateTime.of(year, month, curDay, task.getTime_end().getHour(), task.getTime_end().getMinute());
                TaskDetail weekdayTask = TaskDetail.builder()
                        .employee_name(task.getEmployee_name())
                        .role(task.getRole())
                        .project(task.getProject())
                        .task_category(task.getTask_category())
                        .task_description(task.getTask_description())
                        .time_start(time_start)
                        .time_end(time_end)
                        .build();
                weeklyTasks.add(weekdayTask);
            });
        }
        return weeklyTasks;
    }

    private static int getLastDayOfMonth(int year, int month) throws Exception {
       if(Month.of(month).equals(Month.FEBRUARY)){
           return (isDateValid(year, month, 29)) ? 29 : 28;
       } else {
           return (isDateValid(year, month, 31)) ? 31 : 30;
       }
    }

    private static boolean isDateValid(int year, int month, int day){
        LocalDate localDate;
        try {
            localDate = LocalDate.of(year, month, day);
            return true;
        }catch (Exception ex){
            return false;
        }
    }
}
