package net.garapata.api.attendance.repository;


import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.extern.slf4j.Slf4j;
import net.garapata.api.attendance.model.UserLogin;
import net.garapata.api.attendance.model.UserProfile;
import net.garapata.api.attendance.model.UserRole;
import net.garapata.api.attendance.utils.common.CommonValues;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.json.AutoConfigureJson;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.data.domain.Example;
import org.springframework.data.domain.ExampleMatcher;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.util.Set;


@DataJpaTest
@ExtendWith(SpringExtension.class)
@ActiveProfiles(value = "test")
@AutoConfigureJson
@Slf4j
class UserRoleTests {
    private static final Logger logger = LoggerFactory.getLogger(UserRoleTests.class);
    @Autowired
    UserLoginRepository userLoginRepository;
    @Autowired
    UserProfileRepository userProfileRepository;
    @Autowired
    UserRoleRepository userRoleRepository;
    @Autowired
    ObjectMapper objectMapper;

    @Test
    void testUserRoleCreateRetrieve() throws JsonProcessingException {
        // Given
        UserLogin login = CommonValues.USER_LOGIN_RECORD;
        UserProfile profile = CommonValues.USER_PROFILE_RECORD;
        Set<UserRole> roles = CommonValues.ROLES;
        UserLogin tempLogin = new UserLogin(login.getUsername(), login.getPassword(), login.getEmail());
        tempLogin.setProfile(profile);
        tempLogin.setRoles(roles);

        System.out.println("tempLogin: \n" + objectMapper.writeValueAsString(tempLogin));

        // When
        userLoginRepository.save(login);
        roles.stream().forEach(userRole -> userRole.setLogin(login));
        userRoleRepository.saveAll(roles);
        profile.setLogin(login);
        userProfileRepository.save(profile);
        login.setRoles(roles);
        login.setProfile(profile);
        userLoginRepository.save(login);

        System.out.println("UserLogin, after saving: \n" + objectMapper.writeValueAsString(login));
        System.out.println("UserProfile, after saving: \n " + userProfileRepository.findByFirstNameAndLastName("Joji", "Angas").get());
        ExampleMatcher matcher = ExampleMatcher.matching()
                .withMatcher("user_id", ExampleMatcher.GenericPropertyMatcher.of(ExampleMatcher.StringMatcher.CONTAINING));
        Example<UserRole> role = Example.of(login.getRoles().stream().findFirst().get(), matcher);
        System.out.println("UserRoles, after saving: \n " + userRoleRepository.findAllByUserId(login.getUser_id()));
        // Then

    }

}
