package net.garapata.api.attendance.utils.common;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.context.annotation.Bean;


@TestConfiguration
public class TestContextConfig {

//    @Bean
//    public LoginRepository loginRepository(){
//        return new Login
//    }
//
//    @Bean
//    public ProfileRepository profileRepository(){
//        ProfileRepository profileRepository;
//        return profileRepository;
//    }

    @Bean
    public ObjectMapper objectMapper(){
        ObjectMapper objectMapper=new ObjectMapper();
        objectMapper.configure(SerializationFeature.INDENT_OUTPUT, true);
        objectMapper.configure(DeserializationFeature.ADJUST_DATES_TO_CONTEXT_TIME_ZONE, true);
        objectMapper.configure(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS, true);
        return objectMapper;
    }

//    @Bean
//    public LoginService loginService(){
//        return new LoginServiceImpl();
//    }

//    @Bean
//    public ProfileService profileService(){
//        return new ProfileServiceImpl();
//    }

//    @Bean
//    public LoginRepository loginRepository(){
//        return new LoginRepository();
//    }
//
//    @Bean
//    public ProfileRepository profileRepository(){
//        return new ProfileRepositoryImpl();
//    }

}
