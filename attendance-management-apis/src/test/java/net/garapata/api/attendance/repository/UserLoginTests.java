package net.garapata.api.attendance.repository;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.extern.slf4j.Slf4j;
import net.garapata.api.attendance.model.UserLogin;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.json.AutoConfigureJson;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.data.domain.Example;
import org.springframework.data.domain.ExampleMatcher;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;


@DataJpaTest
@ExtendWith(SpringExtension.class)
@ActiveProfiles(value = "dev")
@AutoConfigureJson
@Slf4j
class UserLoginTests {

    static final Logger logger = LoggerFactory.getLogger(UserLoginTests.class);

    @Autowired
    UserLoginRepository userLoginRepository;
    @Autowired
    ObjectMapper objectMapper;


    @Test
    void testRegisterUserLoginAndFindByUsername() throws JsonProcessingException {
        // Given
        UserLogin login = new UserLogin();
        userLoginRepository.save(login);
        // When
        Optional<UserLogin> savedUserLogin = userLoginRepository.findByUsername("jangas");
        logger.info(objectMapper.writerWithDefaultPrettyPrinter().writeValueAsString(savedUserLogin.get()));
        // Then
        assertTrue(true);
    }

    @Test
    void testRegisterUserLoginWithSameUsername(){
        // Given
        UserLogin login = new UserLogin();
        UserLogin login2 = new UserLogin();
        // When
        userLoginRepository.save(login);
        // Then
        assertThrows(DataIntegrityViolationException.class, ()-> { userLoginRepository.save(login2); });
    }

    @Test
    void testCheckUsernameExists(){
        // Given
        UserLogin login = new UserLogin();
        userLoginRepository.save(login);
        // When
        UserLogin login2 = new UserLogin();
        ExampleMatcher matcher = ExampleMatcher.matching()
                .withMatcher("username", ExampleMatcher.GenericPropertyMatcher::exact);
        Example<UserLogin> userLoginExample = Example.of(login2, matcher);

        boolean usernameExists = userLoginRepository.exists(userLoginExample);
        // Then
        assertFalse(usernameExists);
    }

    @Test
    void testResetUserPassword() throws JsonProcessingException {
        // Given
        UserLogin login = new UserLogin();
        // When
        Optional<UserLogin> savedUserLogin = userLoginRepository.findByUsername("jangas");
        UserLogin beforeSave = savedUserLogin.get();
        savedUserLogin.get().setPassword("p3p2p1p0");
        logger.info(objectMapper.writerWithDefaultPrettyPrinter().writeValueAsString(beforeSave));
        UserLogin updatedUserLogin = new UserLogin();
        updatedUserLogin.setUser_id(beforeSave.getUser_id());
        updatedUserLogin.setCreate_timestamp(beforeSave.getCreate_timestamp());
        logger.info(objectMapper.writerWithDefaultPrettyPrinter().writeValueAsString(updatedUserLogin));
        // Then
        assertNotEquals(beforeSave, updatedUserLogin);
    }

    @Test
    void testDeleteUserLogin() throws JsonProcessingException {
        // Given
        UserLogin login = new UserLogin();
        userLoginRepository.save(login);
        logger.info(objectMapper.writerWithDefaultPrettyPrinter().writeValueAsString(login));
        // When
        userLoginRepository.delete(login);
        // Then
        assertFalse(userLoginRepository.findByUsername("jangas").isPresent());
    }

    @Test
    void testDeleteUserLogin2() throws JsonProcessingException {
        // Given
        UserLogin login = new UserLogin();
        userLoginRepository.save(login);
        logger.info(objectMapper.writerWithDefaultPrettyPrinter().writeValueAsString(login));
        // When
        userLoginRepository.delete(login);
        // Then
        assertFalse(userLoginRepository.findByUsername("jangas").isPresent());
    }
}
