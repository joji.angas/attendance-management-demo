package net.garapata.api.attendance.repository;


import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.extern.slf4j.Slf4j;
import net.garapata.api.attendance.model.UserLogin;
import net.garapata.api.attendance.model.UserProfile;
import org.junit.jupiter.api.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.json.AutoConfigureJson;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.test.context.ActiveProfiles;

import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;


@DataJpaTest
@ActiveProfiles(value = "test")
@EnableJpaRepositories
@AutoConfigureJson
@Slf4j
class UserProfileTests {

    static final Logger logger = LoggerFactory.getLogger(UserProfileTests.class);

    @Autowired
    UserProfileRepository userProfileRepository;

    @Autowired
    ObjectMapper objectMapper;


    @Test
    void testCreateUserProfileAndFindByFirstNameAndLastName() throws JsonProcessingException {
        // Given
//        UserProfile profile = UserProfile.getNewInstance("Joji", "Angas", "Developer", "Test", "2345566778");
//        userProfileRepository.save(profile);
        // When
        Optional<UserProfile> savedProfile = userProfileRepository.findByFirstNameAndLastName("Joji", "Angas");
        logger.info(objectMapper.writerWithDefaultPrettyPrinter().writeValueAsString(savedProfile.get()));
        // Then
        assertTrue(true);
    }

    @Test
    void testFindByLogin() throws JsonProcessingException {
        // Given
        UserLogin login = new UserLogin();
//        UserProfile profile = UserProfile.getNewInstance("Joji", "Angas", "Developer", "Test", "2345566778");
//        profile.setLogin(login);
//        userProfileRepository.save(profile);

        // When
//        Optional<UserProfile> savedProfile = userProfileRepository.findByLogin(login);
//        logger.info(objectMapper.writerWithDefaultPrettyPrinter().writeValueAsString(savedProfile.get()));

        // Then
        assertTrue(true);
//        assertEquals("jangas", savedProfile.get().getLogin().getUsername());
    }

    @Test
    void testCreateDuplicateProfileForSameUserLogin(){
        // Given
        UserLogin login = new UserLogin();
//        UserProfile profile = UserProfile.getNewInstance("Joji", "Angas", "Developer", "Test", "2345566778");
//        profile.setLogin(login);
//        UserProfile profile2 = UserProfile.getNewInstance("John", "Angas", "Developer", "Test", "2345566778");
//        profile2.setLogin(login);

        // When
//        userProfileRepository.save(profile);

        // Then
//        assertThrows(DataIntegrityViolationException.class, ()-> { userProfileRepository.save(profile2); });
    }

    @Test
    void testDeleteUserProfile() throws JsonProcessingException {
        // Given
//        UserProfile profile = UserProfile.getNewInstance("Joji", "Angas", "Developer", "Test", "2345566778");
//        userProfileRepository.save(profile);
//        logger.info(objectMapper.writerWithDefaultPrettyPrinter().writeValueAsString(profile));
        // When
//        userProfileRepository.delete(profile);
        // Then
        assertFalse(userProfileRepository.findByFirstNameAndLastName("Joji", "Angas").isPresent());
    }
}
