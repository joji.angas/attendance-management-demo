package net.garapata.api.attendance.service.security;

import net.garapata.api.attendance.model.UserLogin;
import net.garapata.api.attendance.model.dto.UserLoginDTO;
import net.garapata.api.attendance.repository.UserLoginRepository;
import net.garapata.api.attendance.service.security.UserLoginService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.Optional;


@Service
public class UserLoginServiceImpl implements UserLoginService {

//    public static final Long SYSTEM_USER = Long.parseLong("SYSTEM");

    @Autowired
    private UserLoginRepository userLoginRepository;

    @Override
    public UserLogin register(String username, String password, String email, HttpHeaders headers) {
        if(loginExists(username)) throw new RuntimeException("User account with username "+ username + " exists");
        return createOrUpdate(new UserLoginDTO(username, password, email), headers);
    }

    @Override
    public UserLogin createOrUpdate(UserLoginDTO userLoginDTO, HttpHeaders headers){
        UserLogin login = null;
        if(loginExists(userLoginDTO.getUsername())) {
            UserLogin savedLogin = userLoginRepository.findByUsername(userLoginDTO.getUsername()).get();
            performUpdate(savedLogin, userLoginDTO, headers);
            login = userLoginRepository.save(savedLogin);
        } else {
            UserLogin newUserLogin = new UserLogin(userLoginDTO.getUsername(), userLoginDTO.getPassword(), userLoginDTO.getEmail());
            login = userLoginRepository.save(newUserLogin);
        }
        return login;
    }

    @Override
    public UserLogin changePassword(String username, String newPass, HttpHeaders headers) {
        UserLogin userLogin = new UserLogin();
        return userLogin;
    }

    @Override
    public UserLogin updateEmail(String username, String newEmail, HttpHeaders headers) {
        UserLogin userLogin = new UserLogin();
        return userLogin;
    }

    @Override
    public UserLogin findByUserName(String username, HttpHeaders httpHeaders) {
        UserLogin userLogin = new UserLogin(username, null, null);
        if(loginExists(username)) {
            return userLoginRepository.findByUsername(username).get();
        } else {
            throw new RuntimeException("User with username " + username + " does not exist");
        }
    }

    private UserLogin performUpdate(UserLogin savedLogin, UserLoginDTO userLoginDTO, HttpHeaders headers){
        // reset password block
        if(!savedLogin.getPassword().equals(userLoginDTO.getPassword())){
            savedLogin.setPassword(userLoginDTO.getPassword());
            userLoginRepository.save(savedLogin);
        }
        // change email block
        if(!savedLogin.getEmail().equals(userLoginDTO.getEmail())){
            savedLogin.setEmail(userLoginDTO.getEmail());
        }
        // change account status block
        if(savedLogin.getStatus() != userLoginDTO.getStatus()){
            savedLogin.setStatus(userLoginDTO.getStatus());
        }
        savedLogin.setUpdate_timestamp(LocalDateTime.now());
        savedLogin.setUpdated_by(Long.parseLong(headers.get("user_id").toString()));
        return userLoginRepository.save(savedLogin);
    }

    private boolean loginExists(String username){
        Optional<UserLogin> loginExists = userLoginRepository.findByUsername(username);
        return loginExists.isPresent();
    }
}
