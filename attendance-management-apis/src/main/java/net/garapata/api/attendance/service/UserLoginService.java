package net.garapata.api.attendance.service;

import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import net.garapata.api.attendance.model.UserLogin;
import net.garapata.api.attendance.model.dto.LoginRequest;

import java.net.http.HttpHeaders;


public interface UserLoginService {
    UserLogin register(String username, String password, String email);

    UserLogin changePassword(String username, String newPass, HttpHeaders headers);

    UserLogin updateEmail(String username, String newEmail, HttpHeaders headers);

    void login(LoginRequest loginRequest, HttpServletRequest webRequest, HttpServletResponse webResponse);

    UserLogin findByUserName(String username, HttpHeaders headers);
}
