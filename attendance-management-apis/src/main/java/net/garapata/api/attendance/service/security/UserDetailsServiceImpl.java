package net.garapata.api.attendance.service.security;

import net.garapata.api.attendance.model.UserLogin;
import net.garapata.api.attendance.model.dto.CustomUserDetails;
import net.garapata.api.attendance.repository.UserLoginRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Component;

import java.util.Optional;


@Component
public class UserDetailsServiceImpl implements UserDetailsService {

    @Autowired
    private ObjectMapper objectMapper;
    @Autowired
    private UserLoginRepository userRepository;

    private static final Logger logger = LoggerFactory.getLogger(UserDetailsServiceImpl.class);

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        logger.debug("Entering in loadUserByUsername Method...");
        Optional<UserLogin> login = userLoginRepository.findByUsername(username);
        logger.debug("User found \n {}", login.get());
        logger.debug("UserLogin.roles = {}", login.get().getRoles());
        if(login.isPresent()){
            CustomUserDetails userDetails = null;
            try {
                userDetails = new CustomUserDetails(login.get());
            } catch (Exception e) {
                throw new RuntimeException(e.getCause());
            }
            logger.debug("CustomUserDetails from UserLogin: \n {}", userDetails);
            return userDetails;
        } else {
            logger.error("Username not found {}", username);
            throw new UsernameNotFoundException("Username " + username + " not found");
        }
    }
}
