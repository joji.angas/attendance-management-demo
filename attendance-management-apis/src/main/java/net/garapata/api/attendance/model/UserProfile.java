package net.garapata.api.attendance.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.Objects;


@Data
@Entity
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class UserProfile implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long profile_id;
    private String firstName;
    private String lastName;
    private String department;
    private String position;
    private String phone;

    @JsonIgnore
    @OneToOne(cascade = CascadeType.MERGE)
    @JoinColumn(name = "user_id", nullable = false)
    private UserLogin login;

    public UserProfile(String firstName, String lastName, String department, String position, String phone, UserLogin login){
        this.firstName = firstName;
        this.lastName = lastName;
        this.department = department;
        this.position = position;
        this.phone = phone;
        this.login = login;
    }

    public String getFullName(boolean surnameFirst){
        return (surnameFirst) ? lastName + ", " + firstName : firstName + " " + lastName;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof UserProfile)) return false;
        UserProfile profile = (UserProfile) o;
        return Objects.equals(getProfile_id(), profile.getProfile_id()) && Objects.equals(getFirstName(), profile.getFirstName()) && Objects.equals(getLastName(), profile.getLastName()) && Objects.equals(getLogin(), profile.getLogin());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getProfile_id(), getFirstName(), getLastName(), getLogin());
    }

    @Override
    public String toString() {
        return "UserProfile{" +
                "profile_id=" + profile_id +
                ", firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                ", department='" + department + '\'' +
                ", position='" + position + '\'' +
                ", phone='" + phone + '\'' +
                '}';
    }
}
