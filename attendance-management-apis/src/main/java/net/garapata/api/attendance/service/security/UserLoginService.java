package net.garapata.api.attendance.service.security;

import net.garapata.api.attendance.model.UserLogin;
import net.garapata.api.attendance.model.dto.UserLoginDTO;
import org.springframework.http.HttpHeaders;


public interface UserLoginService {
    UserLogin register(String username, String password, String email, HttpHeaders headers);

    UserLogin changePassword(String username, String newPass, HttpHeaders headers);

    UserLogin updateEmail(String username, String newEmail, HttpHeaders headers);

    UserLogin findByUserName(String username, HttpHeaders headers);

    public UserLogin createOrUpdate(UserLoginDTO userLoginDTO, HttpHeaders headers);
}
