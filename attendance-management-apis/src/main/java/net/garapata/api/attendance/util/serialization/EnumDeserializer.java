package net.garapata.api.attendance.util.serialization;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.deser.std.StdDeserializer;

import java.io.IOException;

import static java.util.Arrays.stream;


public class EnumDeserializer<T> extends StdDeserializer<Integer> {

    public EnumDeserializer(){ this(null); }
    protected EnumDeserializer(Class<?> vc) {
        super(vc);
    }

    @Override
    public Integer deserialize(JsonParser jsonParser, DeserializationContext deserializationContext) throws IOException {
        return Enum.valueOf((Class<Enum>)_valueClass.getDeclaringClass(),jsonParser.getText()).ordinal();
    }
}
