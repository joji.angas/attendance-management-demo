package net.garapata.api.attendance.controller;

import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpRequest;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import java.net.http.HttpResponse;

@Controller
@RequestMapping("/attendance")
public class WelcomeController {

    @PreAuthorize("hasAnyRole('ADMIN', 'ASSOCIATE')")
    @PostMapping("/login/success")
    public ModelAndView loginSuccess(@RequestHeader HttpHeaders headers, HttpRequest request, HttpResponse response){
        return new ModelAndView("index");
    }


}
