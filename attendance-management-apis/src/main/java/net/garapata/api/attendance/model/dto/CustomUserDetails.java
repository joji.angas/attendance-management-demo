package net.garapata.api.attendance.model.dto;

import lombok.Builder;
import lombok.Data;
import net.garapata.api.attendance.model.UserLogin;
import net.garapata.api.attendance.model.UserRole;
import net.garapata.api.attendance.model.enums.RoleType;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;


@Data
@NoArgsConstructor
@AllArgsConstructor
public class CustomUserDetails implements UserDetails {
    private String username;
    private String password;
    Collection<? extends GrantedAuthority> authorities;
    private boolean isEnabled;

    @Builder
    public CustomUserDetails(UserLogin userLogin) {
        this.username = userLogin.getUsername();
        this.password= userLogin.getPassword();
        List<GrantedAuthority> auths = userLogin.getRoles()
                .stream()
                .map(role -> {
                    try {
                        return new SimpleGrantedAuthority(of(role.getRole_type()).getDescription().toUpperCase());
                    } catch (Exception e) {
                        throw new RuntimeException(e.getCause());
                    }
                }).collect(Collectors.toList());
        this.isEnabled = of(userLogin.getStatus()).equals(AccountStatus.ACTIVE);
    }

    @Override
    public boolean isAccountNonExpired() {
        return true;
    }

    @Override
    public boolean isAccountNonLocked() {
        return true;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }

    @Override
    public boolean isEnabled() {
        return isEnabled;
    }

    @Override
    public String toString() {
        return "CustomUserDetails{" +
                "username='" + username + '\'' +
                ", password='" + password + '\'' +
                ", authorities=" + authorities +
                ", isEnabled=" + isEnabled +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof CustomUserDetails)) return false;
        CustomUserDetails that = (CustomUserDetails) o;
        return isEnabled() == that.isEnabled() && Objects.equals(getUsername(), that.getUsername()) && Objects.equals(getPassword(), that.getPassword()) && Objects.equals(getAuthorities(), that.getAuthorities());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getUsername(), getPassword(), getAuthorities(), isEnabled());
    }
}
