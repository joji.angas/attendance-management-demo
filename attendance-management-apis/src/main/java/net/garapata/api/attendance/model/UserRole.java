package net.garapata.api.attendance.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import net.garapata.api.attendance.model.enums.AccountStatus;
import net.garapata.api.attendance.util.serialization.CustomDateDeserializer;
import net.garapata.api.attendance.util.serialization.CustomDateSerializer;
import org.hibernate.annotations.CreationTimestamp;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.Objects;


@Data
@Entity
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class UserRole implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long role_id;

    private int role_type;
    private int role_status;
    @JsonSerialize(using = CustomDateSerializer.class)
    @JsonDeserialize(using = CustomDateDeserializer.class)
    @CreationTimestamp
    private LocalDateTime create_timestamp;
    private Long added_by;
    @JsonSerialize(using = CustomDateSerializer.class)
    @JsonDeserialize(using = CustomDateDeserializer.class)
    private LocalDateTime update_timestamp;
    private Long updated_by;

    @JsonIgnore
    @ManyToOne(cascade = CascadeType.MERGE)
    @JoinColumn(name = "user_id", nullable = false)
    private UserLogin login;

    public UserRole(int role, UserLogin login){
        this.role_type = role;
        this.role_status = AccountStatus.ACTIVE.ordinal();
        this.login = login;
    }

    public UserRole(int role_type, int role_status, UserLogin login){
        this.role_type = role_type;
        this.role_status = role_status;
        this.login = login;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof UserRole)) return false;
        UserRole userRole = (UserRole) o;
        return getRole_type() == userRole.getRole_type() && Objects.equals(getLogin(), userRole.getLogin());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getRole_type(), getLogin());
    }
}
