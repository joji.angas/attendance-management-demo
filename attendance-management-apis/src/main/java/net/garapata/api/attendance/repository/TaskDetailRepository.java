package net.garapata.api.attendance.repository;

import net.garapata.api.attendance.model.TaskDetail;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface TaskDetailRepository extends JpaRepository<TaskDetail, Long> {
}
