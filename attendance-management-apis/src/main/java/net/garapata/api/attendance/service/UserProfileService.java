package net.garapata.api.attendance.service;

import net.garapata.api.attendance.model.UserProfile;
import org.springframework.stereotype.Service;

import java.util.Optional;

public interface UserProfileService {
    Optional<UserProfile> findUserProfileUsingFirstNameAndLastName(String firstName, String lastName);
    UserProfile createUserProfile(UserProfile userProfile);
}
