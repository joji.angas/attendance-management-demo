package net.garapata.api.attendance;

import net.garapata.api.attendance.model.UserLogin;
import net.garapata.api.attendance.model.UserProfile;
import net.garapata.api.attendance.model.UserRole;
import net.garapata.api.attendance.model.enums.AccountStatus;
import net.garapata.api.attendance.model.enums.RoleType;
import net.garapata.api.attendance.repository.UserLoginRepository;
import net.garapata.api.attendance.repository.UserProfileRepository;
import net.garapata.api.attendance.repository.UserRoleRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.core.env.Environment;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

import java.util.Arrays;
import java.util.stream.Collectors;


@SpringBootApplication
@EnableJpaRepositories
public class AttendanceManagementApiApplication {

	private static final Logger logger = LoggerFactory.getLogger(AttendanceManagementApiApplication.class);

	@Autowired
	Environment environment;

	public static void main(String[] args) {

		SpringApplication.run(AttendanceManagementApiApplication.class, args);
//		logger.info("Starting application {}",SpringApplication.)
	}

	@Bean
	public CommandLineRunner loadRowData(UserLoginRepository userLoginRepository, UserRoleRepository userRoleRepository, UserProfileRepository userProfileRepository){
		if(Arrays.stream(environment.getActiveProfiles()).collect(Collectors.toSet()).contains("test") || Arrays.stream(environment.getActiveProfiles()).collect(Collectors.toSet()).contains("dev")) {
			return args -> {
				BCryptPasswordEncoder encoder = new BCryptPasswordEncoder();
				UserLogin login = new UserLogin("jangas", "m4m3m1m0", "jangas@garapata.net");
				login.setStatus(AccountStatus.ACTIVE.ordinal());
				UserRole role = new UserRole(RoleType.ASSOCIATE.ordinal(), login);
				login.addRoles(role);
				userLoginRepository.save(login);
				userRoleRepository.save(role);
				UserProfile profile = new UserProfile("Joji", "Angas", "Test", "Developer", "1234567890", login);
				login.setProfile(profile);
				userProfileRepository.save(profile);
				userLoginRepository.save(login);

			};
		} else { // else, load nothing
			return args -> {
				System.out.println("Other environment profiles used. Not loading test data on the database.");
			};
		}
	}
}
