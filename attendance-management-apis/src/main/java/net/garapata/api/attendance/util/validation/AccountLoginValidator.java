package net.garapata.api.attendance.util.validation;

import jakarta.validation.ConstraintViolation;
import jakarta.validation.Validator;
import jakarta.validation.ValidatorFactory;
import lombok.Getter;
import net.garapata.api.attendance.model.dto.GenericMessageDTO;
import net.garapata.api.attendance.model.dto.UserLoginDTO;
import net.garapata.api.attendance.model.enums.ErrorTypes;
import org.springframework.stereotype.Component;

import java.util.HashSet;
import java.util.Set;

import static jakarta.validation.Validation.buildDefaultValidatorFactory;

@Component
public class AccountLoginValidator {
    AccountLoginValidator instance;
    @Getter
    Set<ConstraintViolation<UserLoginDTO>> violations;

    public AccountLoginValidator(){
        this.violations = new HashSet<ConstraintViolation<UserLoginDTO>>();
    }

    public boolean hasErrors(){
        return !violations.isEmpty();
    }

    public void validateAccountData(UserLoginDTO account){
        ValidatorFactory factory = buildDefaultValidatorFactory();
        Validator validator = factory.getValidator();
        violations = validator.validate(account);
    }

    public GenericMessageDTO buildMessageDetails(){
        GenericMessageDTO validationMessage = GenericMessageDTO.builder()
                .message("A validation error was encountered")
                .hasMoreMessages(true)
                .build();

        // ToDo: cascade through the constraint violation set - Done 05-14-2020
        for(ConstraintViolation<UserLoginDTO> violation: violations){
            GenericMessageDTO fieldValidationErrorMessage = GenericMessageDTO.builder()
                    .message(ErrorTypes.VALIDATION.getErrorTypeMessage() + violation.getMessage())
                    .field(violation.getPropertyPath().toString())
                    .responseCode(ErrorTypes.VALIDATION.getErrorTypeCode())
                    .build();
            validationMessage.put(violation.getPropertyPath().toString(), fieldValidationErrorMessage);
        }
        return validationMessage;
    }

}
