package net.garapata.api.attendance.controller.security;


import net.garapata.api.attendance.model.UserLogin;
import net.garapata.api.attendance.model.UserProfile;
import org.springframework.http.ResponseEntity;

public interface UserLoginApi {
    public ResponseEntity<UserLogin> registerUserLogin(String username, String password, String email);

    public ResponseEntity<UserLogin> findUserByUsername(String username);

    public ResponseEntity<Object> logout();

    public ResponseEntity<UserProfile> getUserProfileWithUserLogin(UserLogin userLogin);
}
