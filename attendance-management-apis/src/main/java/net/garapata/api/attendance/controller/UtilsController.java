package net.garapata.api.attendance.controller;

import jakarta.validation.constraints.NotNull;
import net.garapata.api.attendance.model.dto.GenericDTO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.*;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.support.DefaultMultipartHttpServletRequest;

import java.util.Arrays;
import java.util.Base64;

@RestController
@RequestMapping("/attendance/utils")
public class UtilsController {

    static final Logger logger = LoggerFactory.getLogger(UtilsController.class);

    @Autowired
    BCryptPasswordEncoder bCryptPasswordEncoder;

    @GetMapping("/bcrypt")
    public ResponseEntity<GenericDTO<String>> getBCryptHash(@NotNull @RequestBody String textToEncrypt){
        String bcryptHash = bCryptPasswordEncoder.encode(textToEncrypt);
        GenericDTO<String> responseDTO = new GenericDTO<>(bcryptHash);
        responseDTO.put("bcryptHash", bcryptHash);
        responseDTO.put("responseStatus", HttpStatus.OK.value());
        responseDTO.put("operation", "getBCryptHash");
        responseDTO.put("operationType", "response");
        responseDTO.put("httpMethod", HttpMethod.GET.toString());
        return new ResponseEntity<>(responseDTO, HttpStatusCode.valueOf(HttpStatus.OK.value()));
    }

    @PostMapping(value = "/b64/encode", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<GenericDTO<String>> encodeString(@NotNull @RequestBody DefaultMultipartHttpServletRequest requestObject){
        logger.debug("Servlet Request: \n {}", requestObject);
        logger.debug("request attributes: \n {}", requestObject.getRequest().getAttributeNames().toString());
        String encodedString = Base64.getEncoder().encodeToString("wwooooooooooo".getBytes());
        GenericDTO<String> responseDTO = new GenericDTO<>(encodedString);
        responseDTO.put("encodedString", encodedString);
        responseDTO.put("responseStatus", HttpStatus.OK.value());
        responseDTO.put("operation", "encodeString");
        responseDTO.put("operationType", "response");
        responseDTO.put("httpMethod", HttpMethod.GET.toString());
        return new ResponseEntity<>(responseDTO, HttpStatusCode.valueOf(HttpStatus.OK.value()));
    }

    @PostMapping("/b64/decode")
    public ResponseEntity<GenericDTO<String>> decodeString(@NotNull @RequestBody String textToDecode){
        String decodedString = Arrays.toString(Base64.getDecoder().decode(textToDecode.getBytes()));
        GenericDTO<String> responseDTO = new GenericDTO<>(decodedString);
        responseDTO.put("decodedString", decodedString);
        responseDTO.put("responseStatus", HttpStatus.OK.value());
        responseDTO.put("operation", "decodeString");
        responseDTO.put("operationType", "response");
        responseDTO.put("httpMethod", HttpMethod.GET.toString());
        return new ResponseEntity<>(responseDTO, HttpStatusCode.valueOf(HttpStatus.OK.value()));
    }
}
