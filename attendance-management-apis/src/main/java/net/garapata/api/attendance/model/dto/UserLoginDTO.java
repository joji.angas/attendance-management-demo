package net.garapata.api.attendance.model.dto;

import jakarta.validation.constraints.Email;
import jakarta.validation.constraints.NotBlank;
import lombok.*;
import net.garapata.api.attendance.model.enums.AccountStatus;

import java.io.Serializable;


@Data
@AllArgsConstructor
@NoArgsConstructor
public class UserLoginDTO implements Serializable {
    @NotBlank
    private String username;
    @NotBlank
    private String password;
    @NotBlank
    @Email(message = "Invalid Email format")
    private String email;
    private int status;

    public UserLoginDTO(String username, String password, String email){
        this.username = username;
        this.password = password;
        this.email = email;
        this.status = AccountStatus.INACTIVE.ordinal();
    }

}
