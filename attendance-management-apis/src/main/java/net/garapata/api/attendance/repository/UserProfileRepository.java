package net.garapata.api.attendance.repository;

import net.garapata.api.attendance.model.UserLogin;
import net.garapata.api.attendance.model.UserProfile;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;


@Repository
public interface UserProfileRepository extends JpaRepository<UserProfile, Long> {
    Optional<UserProfile> findByFirstNameAndLastName(String firstName, String lastName);

//    Optional<UserProfile> findByLogin(UserLogin user);
}
