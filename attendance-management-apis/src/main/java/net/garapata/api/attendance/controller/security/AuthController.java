package net.garapata.api.attendance.controller.security;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import jakarta.validation.Valid;
import jakarta.validation.constraints.NotNull;
import lombok.extern.slf4j.Slf4j;
import net.garapata.api.attendance.model.UserLogin;
import net.garapata.api.attendance.model.dto.GenericDTO;
import net.garapata.api.attendance.model.dto.JwtResponse;
import net.garapata.api.attendance.model.dto.UserLoginDTO;
import net.garapata.api.attendance.service.security.JwtService;
import net.garapata.api.attendance.service.security.UserDetailsServiceImpl;
import net.garapata.api.attendance.service.security.UserLoginService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.*;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.web.bind.annotation.*;


@RestController
@RequestMapping("/attendance/api/v1")
@Slf4j
public class AuthController {

    private final Logger logger = LoggerFactory.getLogger(AuthController.class);

    @Autowired
    ObjectMapper objectMapper;
    @Autowired
    AuthenticationManager authenticationManager;

    @Autowired
    UserDetailsServiceImpl userDetailsService;

    @Autowired
    UserLoginService userLoginService;

    @Autowired
    JwtService jwtService;

    @PostMapping(value = "/login", consumes = "*/*", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<GenericDTO<JwtResponse>> authenticateAndGetToken(@RequestBody @NotNull String username, @RequestHeader HttpHeaders headers) throws JsonProcessingException {
        UserDetails fetchedUserDetails = userDetailsService.loadUserByUsername(username);
        Authentication authentication = authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(username, fetchedUserDetails.getPassword(), fetchedUserDetails.getAuthorities()));
        logger.debug("Authentication: \n {}", objectMapper.writeValueAsString(authentication));
        if(authentication.isAuthenticated()){
            JwtResponse jwt = JwtResponse.builder()
                    .accessToken(jwtService.generateToken(username))
                    .build();
            GenericDTO<JwtResponse> jwtResponseDTO = new GenericDTO<>(jwt);
            jwtResponseDTO.put("jwtResponse", objectMapper.writeValueAsString(jwt));
            jwtResponseDTO.put("responseStatus", HttpStatus.OK.value());
            jwtResponseDTO.put("operation", "authenticateAndGetToken");
            jwtResponseDTO.put("operationType", "response");
            jwtResponseDTO.put("httpMethod", HttpMethod.POST.toString());
            return new ResponseEntity<>(jwtResponseDTO, HttpStatusCode.valueOf(HttpStatus.OK.value()));
        } else {
            throw new UsernameNotFoundException("Invalid User login request");
        }
    }

    @PostMapping("/login/register")
    public ResponseEntity<GenericDTO<UserLogin>> registerUserLogin(@RequestBody @Valid UserLoginDTO loginAccount, @RequestHeader HttpHeaders headers){
        return new ResponseEntity<>(new GenericDTO<>(userLoginService.createOrUpdate(loginAccount, headers), null), HttpStatus.OK);
    }

    @PreAuthorize("hasAuthority('ASSOCIATE')")
    @PutMapping("/user/resetpassword")
    public ResponseEntity<GenericDTO> resetPassword(String password, HttpHeaders headers){
        return null;
    }

    @PreAuthorize("hasAuthority('ADMIN')")
    @PutMapping("/user/admin/resetpassword")
    public ResponseEntity<GenericDTO> resetPasswordFor(UserLoginDTO accountFor, HttpHeaders headers){
        return null;
    }

    @PreAuthorize("hasAuthority('ADMIN')")
    @GetMapping("/ping/admin")
    public String test() {
        try {
            return "Has ADMIN role";
        } catch (Exception e){
            throw new RuntimeException(e);
        }
    }

    @PreAuthorize("hasAuthority('ASSOCIATE')")
    @GetMapping("/ping/assoc")
    public String test2() {
        try {
            return "Has ASSOCIATE role";
        } catch (Exception e){
            throw new RuntimeException(e);
        }
    }

    @PreAuthorize("hasAnyRole('ADMIN', 'ASSOCIATE')")
    @GetMapping("/ping/any")
    public String test3() {
        try {
            return "Has ADMIN and ASSOCIATE roles";
        } catch (Exception e){
            throw new RuntimeException(e);
        }
    }
}
