package net.garapata.api.attendance.model.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.*;
import org.springframework.http.HttpStatus;

@Setter
@Getter
public class GenericMessageDTO extends GenericDTO {
    private String message;
    private String field;
    @JsonInclude(JsonInclude.Include.NON_EMPTY)
    private HttpStatus httpResponseCode;
    @JsonInclude(JsonInclude.Include.NON_EMPTY)
    private int responseCode;

    public Object addMessage(String key, Object value) {
        return super.put(key, value);
    }

    public boolean containsMessage(Object value) {
        return super.containsValue(value);
    }

    @Override
    public boolean containsKey(Object key) {
        return super.containsKey(key);
    }

    public GenericMessageDTO(String message) {
        super();
        this.message = message;
    }

    public GenericMessageDTO(String message, int responseCode) {
        super();
        this.message = message;
        this.responseCode = responseCode;
    }

    public GenericMessageDTO(String message, HttpStatus httpResponseCode) {
        super();
        this.message = message;
        this.httpResponseCode = httpResponseCode;
    }

    public GenericMessageDTO(String message, String field) {
        super();
        this.message = message;
        this.field = field;
    }

    public GenericMessageDTO(String message, String field, int responseCode) {
        super();
        this.message = message;
        this.field = field;
        this.responseCode = responseCode;
    }

    @Builder
    public GenericMessageDTO(String message, String field, boolean hasMoreMessages, HttpStatus httpResponseCode, int responseCode){
        super();
        this.message = message;
        this.field = field;
        this.httpResponseCode = httpResponseCode;
        this.responseCode = responseCode;
    }
}
