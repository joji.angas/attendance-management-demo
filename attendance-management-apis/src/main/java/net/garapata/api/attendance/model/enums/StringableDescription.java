package net.garapata.api.attendance.model.enums;

public interface StringableDescription {
    String getDescription() throws RuntimeException;
}
