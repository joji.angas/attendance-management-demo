package net.garapata.api.attendance.model;

import jakarta.persistence.*;
import lombok.*;

import java.io.Serializable;
import java.time.LocalDateTime;

@Getter
@Setter
@Entity
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class TaskDetail implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long task_id;

    private String employee_name;
    @Column(insertable = false, updatable = false)
    private String role;
    private String project;
    private LocalDateTime time_start;
    private LocalDateTime time_end;
    private String task_category;
    private String task_description;

    @ManyToOne
    @JoinColumn(name = "user_id", referencedColumnName = "user_id")
    @JoinColumn(name = "role", referencedColumnName = "position")
    private static UserProfile profile;

}
