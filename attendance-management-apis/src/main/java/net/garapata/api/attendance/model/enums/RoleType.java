package net.garapata.api.attendance.model.enums;


import static java.util.Arrays.stream;

public enum RoleType implements StringableDescription{

    ASSOCIATE, ADMIN;

    @Override
    public String getDescription() throws RuntimeException {
        switch (this) {
            case ADMIN:
                return ADMIN.name().toUpperCase();
            case ASSOCIATE:
                return ASSOCIATE.name().toUpperCase();
            default:
                throw new IllegalArgumentException();
        }
    }

    public static RoleType withRoleIndex(int roleOrdinal){
        return stream(RoleType.values())
                .filter((roleType) -> {
                    return roleType.ordinal() == roleOrdinal;
                })
                .findFirst()
                .get();
    }
}
