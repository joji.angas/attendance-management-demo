package net.garapata.api.attendance.model.enums;

public enum ApprovalStatus implements StringableDescription{

    APPROVED, DECLINED, PENDING_APPROVAL, NEED_INFO, OTHER;

    @Override
    public String getDescription() throws RuntimeException {
        switch (this) {
            case APPROVED:
                return "Approved";
            case DECLINED:
                return "Declined";
            case PENDING_APPROVAL:
                return "Pending Approval";
            case NEED_INFO:
                return "Need Info";
            case OTHER:
                return "Other"; // this should not be possible, placed for compatibility reasons
            default:
                throw new IllegalArgumentException();
        }
    }
}
