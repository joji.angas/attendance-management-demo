package net.garapata.api.attendance.config.security;

import net.garapata.api.attendance.service.security.UserDetailsServiceImpl;
import org.springframework.beans.factory.support.DefaultListableBeanFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.DefaultAuthenticationEventPublisher;
import org.springframework.security.authentication.ProviderManager;
import org.springframework.security.authentication.dao.DaoAuthenticationProvider;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.configuration.ObjectPostProcessorConfiguration;
import org.springframework.security.config.annotation.method.configuration.EnableMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configurers.AbstractHttpConfigurer;
import org.springframework.security.config.annotation.web.configurers.HeadersConfigurer;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.web.SecurityFilterChain;
import org.springframework.security.web.authentication.AuthenticationSuccessHandler;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;


@Configuration
@EnableWebSecurity
@EnableMethodSecurity
public class SecurityConfig {

    @Bean
    public BCryptPasswordEncoder bCryptPasswordEncoder(){
        return new BCryptPasswordEncoder();
    }

    @Bean
    public AuthenticationProvider authenticationProvider(){
        DaoAuthenticationProvider daoProvider = new DaoAuthenticationProvider();
        daoProvider.setUserDetailsService(new UserDetailsServiceImpl());
        daoProvider.setPasswordEncoder(bCryptPasswordEncoder());
        return daoProvider;
    }

    @Bean
    public AuthenticationManager authenticationManager() throws Exception {
        ProviderManager manager = new ProviderManager(authenticationProvider());
        manager.setEraseCredentialsAfterAuthentication(true);
        manager.setAuthenticationEventPublisher(new DefaultAuthenticationEventPublisher());
        AuthenticationManagerBuilder builder = new AuthenticationManagerBuilder(
                new ObjectPostProcessorConfiguration()
                        .objectPostProcessor(new DefaultListableBeanFactory())
        );
        builder.parentAuthenticationManager(manager);
        return builder.build();
    }

    @Bean
    public AuthenticationSuccessHandler authenticationSuccessHandler(){
        return new GaraAuthenticationSuccessHandler();
    }

    @Bean
    public SecurityFilterChain securityFilterChain(HttpSecurity http) throws Exception {

        return http
                .csrf(AbstractHttpConfigurer::disable)
                .authorizeHttpRequests(authorize -> authorize
//                        .requestMatchers("/attendance/api/v1/user/**").authenticated()
//                        .requestMatchers("/attendance/api/v1/ping/**").authenticated()
                        .requestMatchers("/").authenticated()
                        .requestMatchers("/**").permitAll()
                        .requestMatchers("/h2-console/**").permitAll()
                        .requestMatchers("/attendance/api/v1/login**").permitAll()
                        .requestMatchers("/attendance/login**").permitAll()
                        .requestMatchers("/attendance/utils/**").permitAll()
                )
                .formLogin(
                        httpSecurityFormLoginConfigurer ->
                            httpSecurityFormLoginConfigurer
                                    .loginPage("/attendance/login.html")
                                    .loginProcessingUrl("/attendance/api/v1/login")
                                    .successHandler(authenticationSuccessHandler())

                )
                .sessionManagement(httpSecuritySessionManagementConfigurer ->
                    httpSecuritySessionManagementConfigurer.sessionCreationPolicy(SessionCreationPolicy.STATELESS)
                )
                .headers(httpSecurityHeadersConfigurer -> httpSecurityHeadersConfigurer
                        .frameOptions(HeadersConfigurer.FrameOptionsConfig::disable)
                )
                .authenticationProvider(authenticationProvider())
                .authenticationManager(authenticationManager())
                .addFilterAfter(new JwtAuthFilter(), UsernamePasswordAuthenticationFilter.class)
                .build();
    }

}


// reference:
// https://docs.spring.io/spring-security/reference/getting-spring-security.html#getting-maven-boot
// https://medium.com/spring-boot/spring-boot-3-spring-security-6-jwt-authentication-authorization-98702d6313a5

