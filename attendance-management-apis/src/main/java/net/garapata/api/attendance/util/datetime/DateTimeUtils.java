package net.garapata.api.attendance.util.datetime;

import java.time.DayOfWeek;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;
import java.util.Arrays;

public class DateTimeUtils {
    public static String longDesc="EEEE LLLL-dd-yyyy";

    public static String longDescDT="EEEE LLLL-dd-yyyy hh:mm:ss";

    public static String weekMonthDesc="W LLLL";

    public static boolean isWeekend(DayOfWeek dayOfWeek){
        return Arrays.asList(DayOfWeek.SATURDAY, DayOfWeek.SUNDAY).contains(dayOfWeek);
    }

    public static LocalDateTime getFridayForTheWeek(LocalDateTime weekMonday){
        return weekMonday.plusDays(4);
    }

    public static LocalDate getFridayForTheWeek(LocalDate weekMonday){
        return weekMonday.plusDays(4);
    }

    public static long getDateTimeDifference(LocalDateTime timeStart, LocalDateTime timeEnd, ChronoUnit durationDescriptor){
        return timeStart.until(timeEnd, durationDescriptor);
    }

    public static String getLongDescription(LocalDateTime inDate){
        return inDate.format(DateTimeFormatter.ofPattern(longDescDT));
    }

    public static String getWeekOfMonthDescription(LocalDateTime inDate){
        String[] weekMonth = inDate.format(DateTimeFormatter.ofPattern(weekMonthDesc)).split(" ");
        return "Week " + weekMonth[0] + " of " + weekMonth[1];
    }

    public static String getLongDescription(LocalDate inDate){
        return inDate.format(DateTimeFormatter.ofPattern(longDesc));
    }

    public static String getWeekOfMonthDescription(LocalDate inDate){
        String[] weekMonth = inDate.format(DateTimeFormatter.ofPattern(weekMonthDesc)).split(" ");
        return "Week " + weekMonth[0] + " of " + weekMonth[1];
    }

//    public static LocalDateTime getRelativeWeekdayFromDate(LocalDateTime refDateTime, DayOfWeek dayOfWeek){
//        LocalDateTime relativeWeekDay=null;
//        return () -> (refDateTime.getDayOfWeek().equals(dayOfWeek)) ?
////                    System.out.println("Naglolokohan pala tayo dito eh!");
//                    refDateTime :
//                {
//                    while(!relativeWeekDay.getDayOfWeek().equals(dayOfWeek)){
//                        LocalDateTime.now();
//                    }
//                };
//
//    }
}
