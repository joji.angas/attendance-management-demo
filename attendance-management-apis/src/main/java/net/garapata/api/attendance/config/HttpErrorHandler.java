package net.garapata.api.attendance.config;

import net.garapata.api.attendance.model.dto.GenericDTO;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.HttpStatusCode;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.client.HttpServerErrorException;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import java.net.http.HttpHeaders;


@ControllerAdvice
public class HttpErrorHandler extends ResponseEntityExceptionHandler {

//    @ExceptionHandler({ MethodArgumentNotValidException.class })
//    public ResponseEntity<GenericDTO<MethodArgumentNotValidException>> handleValidationErrors(MethodArgumentNotValidException ex, HttpHeaders headers, WebRequest request){
//        GenericDTO<MethodArgumentNotValidException> valError = new GenericDTO<>(ex);
//        valError.put("errorType", "Validation Errors");
//        valError.put("cause", ex.getCause());
//        valError.put("message", ex.getMessage());
//        ex.getBindingResult().getFieldErrors().forEach(fieldError -> valError.put(fieldError.getField(), fieldError.getDefaultMessage()) );
//        return new ResponseEntity<>(valError, new LinkedMultiValueMap<>(headers.map()), HttpStatusCode.valueOf(HttpStatus.NOT_FOUND.value()));
//    }

    @ExceptionHandler({ BadCredentialsException.class })
    public ResponseEntity<GenericDTO<BadCredentialsException>> handleAuthenticationErrors(BadCredentialsException badEx, HttpHeaders headers, WebRequest webRequest){
        GenericDTO<BadCredentialsException> badCredentialsError = new GenericDTO<>(badEx);
        badCredentialsError.put("errorType", "Bad Credentials");
        badCredentialsError.put("cause", badEx.getCause());
        badCredentialsError.put("message", badEx.getMessage());
        badCredentialsError.put("responseStatus", HttpStatus.BAD_REQUEST.name());
        badCredentialsError.put("operation", "authenticateAndGetToken");
        badCredentialsError.put("operationType", "response");
        badCredentialsError.put("httpMethod", HttpMethod.POST.toString());
        return new ResponseEntity<>(badCredentialsError, HttpStatusCode.valueOf(HttpStatus.BAD_REQUEST.value()));
    }

    @ExceptionHandler({ UsernameNotFoundException.class })
    public ResponseEntity<GenericDTO<Exception>> handleResourceNotFoundErrors(UsernameNotFoundException ex, HttpHeaders headers, WebRequest webRequest){
        GenericDTO<Exception> resNotFoundError = new GenericDTO<>(ex);
        resNotFoundError.put("errorType", "Resource Not Found");
        resNotFoundError.put("cause", ex.getCause());
        resNotFoundError.put("message", ex.getMessage());
        return new ResponseEntity<>(resNotFoundError, new LinkedMultiValueMap<>(headers.map()), HttpStatusCode.valueOf(HttpStatus.NOT_FOUND.value()));
    }

    @ExceptionHandler({ AccessDeniedException.class })
    public ResponseEntity<GenericDTO<AccessDeniedException>> handleAccessDeniedErrors(AccessDeniedException ex, HttpHeaders headers, WebRequest webRequest){
        GenericDTO<AccessDeniedException> acDenError = new GenericDTO<>(ex);
        acDenError.put("errorType", "Access Denied");
        acDenError.put("cause", ex.getCause());
        acDenError.put("message", ex.getMessage());
        return new ResponseEntity<>(acDenError, new LinkedMultiValueMap<>(headers.map()), HttpStatusCode.valueOf(HttpStatus.FORBIDDEN.value()));
    }

    @ExceptionHandler({ HttpServerErrorException.InternalServerError.class })
    public ResponseEntity<GenericDTO<Exception>> handleInternalServerErrors(Exception ex, HttpHeaders headers, WebRequest webRequest){
        GenericDTO<Exception> intSrvError =  new GenericDTO<>(ex);
        intSrvError.put("errorType", "Internal Server Error");
        intSrvError.put("cause", ex.getCause());
        intSrvError.put("message", ex.getMessage());
        return new ResponseEntity<>(intSrvError, new LinkedMultiValueMap<>(headers.map()), HttpStatusCode.valueOf(HttpStatus.INTERNAL_SERVER_ERROR.value()));
    }

    @ExceptionHandler({ RuntimeException.class })
    public ResponseEntity<GenericDTO<RuntimeException>> handleGenericRuntimeExceptions(RuntimeException rEx, HttpHeaders headers, HttpStatusCode statusCode, WebRequest webRequest){
        GenericDTO<RuntimeException> runtimeException = new GenericDTO<>(rEx);
        runtimeException.put("errorType", "Runtime Exception");
        runtimeException.put("cause", rEx.getCause());
        runtimeException.put("message", rEx.getMessage());
        return new ResponseEntity<>(runtimeException, new LinkedMultiValueMap<>(headers.map()), statusCode);
    }

    @ExceptionHandler({ Exception.class })
    public ResponseEntity<GenericDTO<Exception>> handleDefaultExceptions(Exception ex, HttpHeaders headers, HttpStatusCode statusCode, WebRequest webRequest){
        GenericDTO<Exception> exception = new GenericDTO<>(ex);
        exception.put("errorType", "Unknown Exception");
        exception.put("cause", ex.getCause());
        exception.put("message", ex.getMessage());
        return new ResponseEntity<>(exception, new LinkedMultiValueMap<>(headers.map()), statusCode);
    }
}
