package net.garapata.api.attendance.controller;

import lombok.extern.slf4j.Slf4j;
import net.garapata.api.attendance.model.UserLogin;
import net.garapata.api.attendance.model.UserProfile;
import net.garapata.api.attendance.service.OAuth2UserLoginService;
import net.garapata.api.attendance.service.UserProfileService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Collections;
import java.util.Map;


@RestController
@RequestMapping("/user")
@Slf4j
public class UserLoginController implements UserLoginApi {

    static final Logger logger = LoggerFactory.getLogger(UserLoginController.class);
    @Autowired
    OAuth2UserLoginService oAuth2UserLoginService;
    @Autowired
    UserProfileService userProfileService;

    @GetMapping("/login")
    public Map<String, Object> userLogin(@AuthenticationPrincipal UserLogin principal){
        return Collections.singletonMap("name", principal.getAttribute("name"));
    }

    @PostMapping()
    @Override
    public ResponseEntity<UserLogin> registerUserLogin(String username, String password, String email) {
        return null;
    }

    @Override
    public ResponseEntity<UserLogin> findUserByUsername(String username) {
        return null;
    }


    @Override
    public ResponseEntity<Object> logout() {
        return null;
    }

    @Override
    public ResponseEntity<UserProfile> getUserProfileWithUserLogin(UserLogin userLogin) {
        return null;
    }

}
