package net.garapata.api.attendance.model.dto;

import net.garapata.api.attendance.model.UserLogin;
import net.garapata.api.attendance.model.UserProfile;
import net.garapata.api.attendance.model.UserRole;

import java.util.List;

public class UserProfileDTO {
    private UserLogin userLogin;
    private UserProfile userProfile;
    private List<UserRole> userRoles;
}
