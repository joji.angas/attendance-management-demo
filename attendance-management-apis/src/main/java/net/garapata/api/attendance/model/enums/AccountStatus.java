package net.garapata.api.attendance.model.enums;

import java.util.Arrays;


public enum AccountStatus implements StringableDescription{
    ACTIVE, INACTIVE, DISABLED, PURGED, OTHER;

    @Override
    public String getDescription() throws RuntimeException {
        switch (this) {
            case ACTIVE:
                return "Active";
            case INACTIVE:
                return "Inactive";
            case DISABLED:
                return "Disabled";
            case PURGED:
                return "Purged"; // for batch deletion
            case OTHER:
                return "Other"; // this should not be possible, placed for compatibility reasons
            default:
                throw new IllegalArgumentException();
        }
    }

    public static AccountStatus of(int statusOrdinal) throws Exception {
        if(Arrays.stream(AccountStatus.values()).filter(status -> status.ordinal()==statusOrdinal).findFirst().isPresent()) {
            return Arrays.stream(AccountStatus.values()).filter(status -> status.ordinal() == statusOrdinal).findFirst().get();
        } else {
            throw new Exception("Account status with index " + statusOrdinal + " does not exist");
        }
    }
}
