package net.garapata.api.attendance.config;

import com.fasterxml.jackson.core.util.DefaultPrettyPrinter;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import org.modelmapper.ModelMapper;
import org.modelmapper.convention.MatchingStrategies;
import org.springframework.boot.web.embedded.tomcat.TomcatServletWebServerFactory;
import org.springframework.boot.web.server.WebServerFactoryCustomizer;
import org.springframework.boot.web.servlet.server.ConfigurableServletWebServerFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;

import java.io.File;
import java.util.Base64;


@Configuration
@Profile(value = {"test", "dev", "prod"})
public class BeansConfig {

    @Bean
    public ObjectMapper objectMapper(){
        return new ObjectMapper()
                .configure(SerializationFeature.INDENT_OUTPUT,true)
                .configure(SerializationFeature.FAIL_ON_EMPTY_BEANS, false)
                .setDefaultPrettyPrinter(new DefaultPrettyPrinter());
    }

    @Bean
    public WebServerFactoryCustomizer<ConfigurableServletWebServerFactory> webServerFactoryCustomizer(){
        return factory -> {
            factory = new TomcatServletWebServerFactory();
            factory.setContextPath("");
            factory.setDisplayName("attendance");
            factory.setDocumentRoot(new File("./static"));
        };
    }

    @Bean
    public ModelMapper modelMapper(){
        ModelMapper modelMapper = new ModelMapper();
        modelMapper.getConfiguration().setMatchingStrategy(MatchingStrategies.STANDARD);
        return modelMapper;
    }

}
