package net.garapata.api.attendance.service;

import net.garapata.api.attendance.model.UserProfile;
import net.garapata.api.attendance.repository.UserProfileRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class UserProfileServiceImpl implements UserProfileService {

    @Autowired
    UserProfileRepository userProfileRepository;

    @Override
    public Optional<UserProfile> findUserProfileUsingFirstNameAndLastName(String firstName, String lastName) {
        return userProfileRepository.findByFirstNameAndLastName(firstName, lastName);
    }

    @Override
    public UserProfile createUserProfile(UserProfile userProfile) {
        return userProfileRepository.save(userProfile);
    }
}
