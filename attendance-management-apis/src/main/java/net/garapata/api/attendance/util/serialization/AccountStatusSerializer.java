package net.garapata.api.attendance.util.serialization;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.fasterxml.jackson.databind.ser.std.StdSerializer;
import net.garapata.api.attendance.model.enums.AccountStatus;

import java.io.IOException;
import java.util.Arrays;

public class AccountStatusSerializer extends StdSerializer<Integer> {

    public AccountStatusSerializer() {
        this(null);
    }

    protected AccountStatusSerializer(Class<Integer> t) {
        super(t);
    }

    @Override
    public void serialize(Integer integer, JsonGenerator jsonGenerator, SerializerProvider serializerProvider) throws IOException {
        jsonGenerator.writeString(Arrays.stream(AccountStatus.values())
                .filter((status) -> status.ordinal() == integer)
                .findFirst().get().name()
        );
    }
}