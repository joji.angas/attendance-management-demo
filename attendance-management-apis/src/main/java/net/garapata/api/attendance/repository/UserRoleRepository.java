package net.garapata.api.attendance.repository;

import net.garapata.api.attendance.model.UserRole;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface UserRoleRepository extends JpaRepository<UserRole, Long> {

//    @Query("select r from UserRole r where r.user_id = ?1")
//    Optional<List<UserRole>> findAllByUserId(Long user_id);
}
