package net.garapata.api.attendance.model;

import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import lombok.*;

import java.time.LocalDateTime;


@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode
@Entity
@Setter
@Getter
@Builder
public class Leave {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long leave_id;
    private Long user_id;
    private LocalDateTime leave_start;
    private LocalDateTime leave_end;
    private int leave_type;
    private int leave_duration;

    private Long approved_by;
    private Long updated_by;
    private int leave_status;
    private String remarks;
}
