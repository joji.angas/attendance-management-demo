package net.garapata.api.attendance.model;

import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import lombok.*;

import java.time.LocalDateTime;


@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode
@Entity
@Setter
@Getter
@Builder
public class Attendance {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long attendance_id;
    private LocalDateTime clock_in;
    private LocalDateTime clock_out;
    private Long user_id;

    private Long approved_by;
    private Long updated_by;
    private int attendance_status;
    private String remarks;
}
