package net.garapata.api.attendance.model.dto;

import com.fasterxml.jackson.annotation.*;
import lombok.AllArgsConstructor;
import lombok.Getter;
import org.springframework.lang.Nullable;

import java.util.HashMap;
import java.util.Map;


@AllArgsConstructor
public class GenericDTO<T> {
    @Getter
    @JsonInclude(JsonInclude.Include.NON_EMPTY)
    @JsonProperty("attributes")
    private Map<String, Object> attributes;
    @JsonIgnore()
    @Nullable
    private final T relatedType;

    public GenericDTO(){
        this.relatedType = (T) new Object();
    }

    public GenericDTO(T dtoType, Map<String,Object> attributes){
        this.relatedType = dtoType;
        this.attributes = (attributes == null || attributes.isEmpty()) ? new HashMap<>() : attributes;
    }

    public GenericDTO(T dtoType){
        this.relatedType = dtoType;
        this.attributes = new HashMap<>();
    }

    public void setAttributes(Map<String, Object> toAttributes) {
        this.attributes = (toAttributes == null || toAttributes.isEmpty()) ? new HashMap<>() : toAttributes;
    }

    @JsonAnySetter
    public Object put(String key, Object value) {
        return attributes.put(key, value);
    }

    public int size() {
        return attributes.size();
    }

    @JsonIgnore
    public boolean isEmpty() {
        return attributes.isEmpty();
    }

    public boolean containsKey(Object key) {
        return attributes.containsKey(key);
    }

    public boolean containsValue(Object value) {
        return attributes.containsValue(value);
    }

    public Object get(Object key) {
        return attributes.get(key);
    }

    public Object remove(Object key) {
        return attributes.remove(key);
    }

    public void putAll(Map<? extends String, ?> m) {
        attributes.putAll(m);
    }

//    @JsonCreator
//    public GenericDTO(@JsonProperty("generic_attributes") Map<String, Object> attributes, @Nullable T relatedType){
//        this.attributes = attributes;
//        relatedType = relatedType;
//    }
}
