package net.garapata.api.attendance.util.serialization;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.fasterxml.jackson.databind.ser.std.StdSerializer;

import java.io.IOException;


public class EnumSerializer<T> extends StdSerializer<T> {

    public EnumSerializer(){ this(null); }

    protected EnumSerializer(Class<T> t) {
        super(t);
    }



    @Override
    public void serialize(T t, JsonGenerator jsonGenerator, SerializerProvider serializerProvider) throws IOException {
        if(t instanceof Enum)
            t = (T) Enum.valueOf(((Enum<?>) t).getDeclaringClass(), ((Enum<?>) t).name());
        jsonGenerator.writeString(t.toString());
    }
}
