package net.garapata.api.attendance.service;

import net.garapata.api.attendance.model.TaskDetail;

import java.util.List;
import java.util.Optional;

public interface TaskDetailService {
    Optional<TaskDetail> createTaskEntry(TaskDetail entry);
    Optional<TaskDetail> updateTaskEntry(Long taskEntryId);
    Boolean deleteTaskEntry(Long taskEntryId);

    Optional<List<TaskDetail>> fetchTodayTotalEntries();

    Optional<List<TaskDetail>> fetchThisWeekTotalEntries();

    Optional<List<TaskDetail>> fetchThisMonthTotalEntries();
}
