package net.garapata.api.attendance.model.enums;

public enum ErrorTypes {
    APPLICATION, VALIDATION, DATABASE, WEB_REQUEST, OTHER;

    public final int getErrorTypeCode(){
        int errorCode = 0;
        switch (this){
            case APPLICATION:
                errorCode = 15;
                break;
            case VALIDATION:
                errorCode = 24;
                break;
            case DATABASE:
                errorCode = 42;
                break;
            case WEB_REQUEST:
                errorCode = 41;
                break;
            case OTHER:
                errorCode = 69;
                break;
            default:
                errorCode = 99;
                break;
        }
        return errorCode;
    }

    public final String getErrorTypeMessage(){
        switch (this) {
            case APPLICATION:
                return "An application error occurred: ";
            case VALIDATION:
                return "A field validation error occurred: ";
            case DATABASE:
                return "A database error occurred: ";
            case WEB_REQUEST:
                return "There was an error processing the web request: ";
            default:
                return "An unknown error occurred: ";
        }
    }

}
