package net.garapata.api.attendance.model;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import jakarta.persistence.*;
import lombok.*;
import net.garapata.api.attendance.model.enums.RoleType;
import net.garapata.api.attendance.service.security.UserDetailsServiceImpl;
import net.garapata.api.attendance.util.serialization.AccountStatusDeserializer;
import net.garapata.api.attendance.util.serialization.AccountStatusSerializer;
import net.garapata.api.attendance.util.serialization.CustomDateDeserializer;
import net.garapata.api.attendance.util.serialization.CustomDateSerializer;
import org.hibernate.annotations.CreationTimestamp;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

import static net.garapata.api.attendance.model.enums.AccountStatus.INACTIVE;


@Data
@Entity
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class UserLogin implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long user_id;
    @Column(unique = true, nullable = false)
    private String username;
    @Column(nullable = false)
    @JsonIgnore
    private String password;
    @Column(nullable = false)
    private String email;
    @JsonSerialize(using = AccountStatusSerializer.class)
    @JsonDeserialize(using = AccountStatusDeserializer.class)
    @Column
    private int status;
    @JsonSerialize(using = CustomDateSerializer.class)
    @JsonDeserialize(using = CustomDateDeserializer.class)
    @CreationTimestamp
    private LocalDateTime create_timestamp;
    @JsonSerialize(using = CustomDateSerializer.class)
    @JsonDeserialize(using = CustomDateDeserializer.class)
    private LocalDateTime update_timestamp;
    private Long updated_by;

    @JsonIgnore
    @OneToOne(mappedBy = "login")
    private UserProfile profile;

    @JsonIgnore
    @OneToMany(fetch = FetchType.EAGER, mappedBy = "login")
    private Set<UserRole> roles;

    public UserLogin(String username, String password, String email){
        this.username = username;
        this.password = password;
        this.email = email;
        this.status = INACTIVE.ordinal();
    }

    @Override
    public String toString() {
        return "UserLogin{" +
                "user_id=" + user_id +
                ", username='" + username + '\'' +
                ", password='" + password + '\'' +
                ", email='" + email + '\'' +
                ", status=" + status +
                '}';
    }

    public void addRoles(UserRole... roleList){
        if(this.roles == null) this.roles = new HashSet<>();
        this.roles.addAll(Set.of(roleList));
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof UserLogin)) return false;
        UserLogin userLogin = (UserLogin) o;
        return Objects.equals(getUser_id(), userLogin.getUser_id()) && Objects.equals(getUsername(), userLogin.getUsername());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getUser_id(), getUsername());
    }
}
