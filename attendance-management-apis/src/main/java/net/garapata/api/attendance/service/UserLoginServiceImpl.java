package net.garapata.api.attendance.service;

import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import net.garapata.api.attendance.model.UserLogin;
import net.garapata.api.attendance.model.dto.LoginRequest;
import net.garapata.api.attendance.repository.UserLoginRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Example;
import org.springframework.data.domain.ExampleMatcher;
import org.springframework.stereotype.Service;

import java.net.http.HttpHeaders;
import java.time.LocalDateTime;


@Service
public class UserLoginServiceImpl implements UserLoginService {
    @Autowired
    private final UserLoginRepository userLoginRepository;

    public UserLoginServiceImpl(UserLoginRepository loginRepository){
        this.userLoginRepository = loginRepository;
    }

    @Override
    public UserLogin register(String username, String password, String email) {
        if(loginExists(username)) throw new RuntimeException("User account with username "+ username + " exists");
        return null;
    }

    @Override
    public UserLogin changePassword(String username, String newPass, HttpHeaders headers) {
        UserLogin userLogin = new UserLogin();
        return userLogin;
    }

    @Override
    public UserLogin updateEmail(String username, String newEmail, HttpHeaders headers) {
        UserLogin userLogin = new UserLogin();
        return userLogin;
    }

    @Override
    public void login(LoginRequest loginRequest, HttpServletRequest webRequest, HttpServletResponse webResponse) {
    }

    @Override
    public UserLogin findByUserName(String username, HttpHeaders httpHeaders) {
        UserLogin userLogin = new UserLogin();
        return userLogin;
    }

    private UserLogin performUpdate(UserLogin updatedLogin, HttpHeaders headers){
        updatedLogin.setUpdate_timestamp(LocalDateTime.now());
        updatedLogin.setUpdated_by(Long.parseLong(headers.map().get("user_id").toString()));
        return userLoginRepository.save(updatedLogin);
    }

    private boolean loginExists(String username){
        UserLogin loginCheck = new UserLogin(username, null, null);
        ExampleMatcher matcher = ExampleMatcher.matching()
                .withIgnorePaths("password", "email", "status")
                .withMatcher("username", ExampleMatcher.GenericPropertyMatcher.of(ExampleMatcher.StringMatcher.EXACT));
        Example<UserLogin> loginExample = Example.of(loginCheck, matcher);
        return userLoginRepository.loginExists(loginExample);
    }
}
