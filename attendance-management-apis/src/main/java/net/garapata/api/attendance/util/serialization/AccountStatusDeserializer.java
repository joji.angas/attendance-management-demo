package net.garapata.api.attendance.util.serialization;

import com.fasterxml.jackson.core.JacksonException;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.deser.std.StdDeserializer;
import net.garapata.api.attendance.model.enums.AccountStatus;

import java.io.IOException;

public class AccountStatusDeserializer extends StdDeserializer<Integer> {

    public AccountStatusDeserializer(Class<?> vc) {
        super(vc);
    }

    public AccountStatusDeserializer() {
        this(null);
    }


    @Override
    public Integer deserialize(JsonParser jsonParser, DeserializationContext deserializationContext) throws IOException, JacksonException {
        return AccountStatus.valueOf(jsonParser.getText()).ordinal();
    }
}
