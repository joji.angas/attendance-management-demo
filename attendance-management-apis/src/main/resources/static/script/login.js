
async function encodeRequestDetails(loginRequest){
    const base64EncodeEndpoint = 'http://localhost:8085/attendance/utils/b64/encode'
    const response = await fetch(base64EncodeEndpoint, {
            method : "GET",
            headers: {
                  "Content-Type": "application/json",
                  "Content-Type": "application/x-www-form-urlencoded"
            },
            body : new FormData(loginRequest)
        });
    console.log(response.json());
    return response.json();
}

function doAuthenticate(encodedLoginRequest){

}

function doEncryptAndSubmit(formId){
    var formData = new FormData(document.forms.formId);
    const loginRequest = formData.fromEntries();
    const encodedLoginRequest = encodeRequestDetails(loginRequest);
    // do more processing here
}